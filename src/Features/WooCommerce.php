<?php


namespace RelayPay\Features;

class WooCommerce {
	public function __construct() {
		add_filter( 'woocommerce_payment_gateways', array( $this, 'add_gateway' ) );
	}

	/**
	 * Add gateway
	 *
	 * @param array $gateways Array of available gateways.
	 *
	 * @return array
	 */
	public function add_gateway( array $gateways ): array {
		$gateways[] = Gateway::class;

		return $gateways;
	}

}
