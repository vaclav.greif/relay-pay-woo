<?php

use RelayPayDeps\DI\Definition\Helper\CreateDefinitionHelper;
use RelayPayDeps\Wpify\CustomFields\CustomFields;
use RelayPayDeps\Wpify\Model\Manager;
use RelayPayDeps\Wpify\PluginUtils\PluginUtils;
use RelayPayDeps\Wpify\Template\WordPressTemplate;

return array(
	CustomFields::class      => ( new CreateDefinitionHelper() )
		->constructor( plugins_url( 'deps/wpify/custom-fields', __FILE__ ) ),
	WordPressTemplate::class => ( new CreateDefinitionHelper() )
		->constructor( array( __DIR__ . '/templates' ), 'relaypay' ),
	PluginUtils::class       => ( new CreateDefinitionHelper() )
		->constructor( __DIR__ . '/relaypay.php' ),
	Manager::class => ( new CreateDefinitionHelper() )
		->constructor( [] )
);
